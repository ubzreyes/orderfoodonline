*** Settings ***
Default Tags      FoodTest
Library           Selenium2Library
Library           String
Library           Collections
Library           OperatingSystem
Resource          food_panda_keywords.robot
Suite Setup       Open Browser    https://www.foodpanda.ph   chrome
Suite Teardown    Close ALl Browsers
*** Variables ***
${city}       Taguig City
${address}    Trade and Financial Tower, Taguig City, NCR, Philippines

*** Test Cases ***
User Has Successfully Navigated To Restaurants Nearby
    Given User Is In FoodPanda HomePage
    When User Submits Location Details    ${city}    ${address}
    Then User Should Be Redirected To Restaurant List

User Has Successfully Visited Restaurant With Fastest Delivery And Have Deals
   Given User Is In Restaurants List Page
   When User Filters Restaurants by "Deals"
   And User Sorted Restaurants by "Fastest Delivery"
   And User Visited Number "1" On The List
   Then User Should Be Redirected To Restaurant Details Page

User Has Successfully Placed Order
   Given User Is In Restaurant Details Page
   When User Adds Random Order Until Minimum Delivery Is Met
   And User Saves Orders To CSV
   And User Proceeds To Checkout