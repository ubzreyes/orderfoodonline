from setuptools import setup

setup(
	name='firstcircle',
	version='1.0',
	description='Exam',
	author='Ubz Reyes',
	author_email='eubertreyes@gmail.com',
	install_requires=[
		'robotframework',
		'robotframework-selenium2library',
		'chromedriver_installer'
	]
)