*** Keywords ***

#-------------------------#
#          GIVEN          #
#-------------------------#
User Is In FoodPanda HomePage
    Wait Until Keyword Succeeds    10 sec    2 sec
    ...    Location Should Contain    https://www.foodpanda.ph/

User Is In Restaurants List Page
    Wait Until Keyword Succeeds    10 sec    2 sec
    ...    Location Should Contain    /restaurants

User Is In Restaurant Details Page
    Wait Until Keyword Succeeds    10 sec    2 sec
    ...    Location Should Contain    /restaurant/

#-------------------------#
#           WHEN          #
#-------------------------#
User Submits Location Details
	[Arguments]    ${p_city}    ${p_address}
	Input Text    css=#wrapper-element-1 .form-control.twitter-typeahead.tt-input    Taguig City
	Input Text    id=address    Trade and Financial Tower, Taguig City, NCR, Philippines
	Wait Until Element Is Visible    css=div.address .tt-dropdown-menu
	Click Element    id=button

User Filters Restaurants by "${filter}"
    Wait Until Element Is Visible    css=aside.vendor-filter
    Select From List By Value    name=sort    minimum_delivery_time_asc

User Sorted Restaurants by "${sort}"
    Wait Until Element Is Visible    css=aside.vendor-filter
    Click Element    css=label[for="has_discount"]

User Visited Number "${index}" On The List
    Wait Until Element Is Visible    css=section.vendor-list.js-infscroll-container
    Click Element    css=a[data-clicked_restaurant_position="${index}"]

User Adds Random Order Until Minimum Delivery Is Met
    :FOR    ${INDEX}    IN RANGE    0    1000
    \    ${menuItem} =    Get Random Menu Item
    \    Wait Until Element Is Visible    ${menuItem}
    \	 Click Element    ${menuItem}

    \	 ${isChoicesVisible} =    Run Keyword And Return Status    Wait Until Element Is Visible    css=ul[data-maxselect]
    \    Run Keyword If    ${isChoicesVisible}    Select From Choices Randomly

    \	 ${isSubmitVisible} =    Run Keyword And Return Status   Wait Until Element Is Visible   css=#cart_product_skeleton_submit
    \    Run Keyword If    ${isSubmitVisible}    Click Element  css=#cart_product_skeleton_submit

    \    ${isError} =    Run Keyword And Return Status    Wait Until Element Is Visible    css=div.cart__checkout div.cart__message
    \    Log    ${isError}
    \    Exit For Loop If    "${isError}"=="False"

User Proceeds to Checkout
    Click Element    css=a.btn-primary.js-checkout-btn

User Saves Orders To CSV
    ${content} =    Get Text    css=div.cart__content
    Create File    \file.csv    ${content}
#-------------------------#
#           THEN          #
#-------------------------#
User Should Be Redirected To Restaurant List
    Wait Until Element Is Visible    css=section.vendor-list.js-infscroll-container

User Should Be Redirected To Restaurant Details Page
    Wait Until Element Is Visible    css=main.js-sticky-height-calculate-container

#-------------------------#
#    INTERNAL KEYWORDS    #
#-------------------------#
Get Random Menu Item
    ${elements} =    Get Webelements    css=article.menu-item div.menu-item__variations-container
    ${count} =    Get Length    ${elements}
    ${index} =    Evaluate    random.randint(1, ${count})    modules=random
    ${element} =    Get From List    ${elements}    ${index}
    [Return]    ${element}

Select From Choices Randomly
    ${elements} =    Get Webelements    css=ul[data-maxselect]
    ${count} =    Get Length    ${elements}
    ${count} =    Evaluate   ${count} + 2
    :FOR    ${INDEX}    IN RANGE    2    ${count}
    \    ${elements} =    Get Webelements    css=form div.choices-toppings__wrapper:nth-of-type(${INDEX}) ul li
    \    Show All Choices    ${INDEX}
    \    ${count} =    Get Length    ${elements}
    \    ${randomNum} =    Evaluate    random.randint(1, ${count})    modules=random
    \    Click Element    css=form div.choices-toppings__wrapper:nth-of-type(${INDEX}) ul li:nth-of-type(${randomNum}) label

Show All Choices
    [Arguments]    ${p_choicesindex}
    ${isVisible} =    Run Keyword and Return Status
    ...    Element Should Be Visible    css=form div.choices-toppings__wrapper:nth-of-type(${p_choicesindex}) span.choices-toppings__elements__container--show-more--text
    Run Keyword If     ${isVisible}
    ...    Click Element    css=form div.choices-toppings__wrapper:nth-of-type(${p_choicesindex}) span.choices-toppings__elements__container--show-more--text